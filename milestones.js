#!/usr/bin/env node
/* eslint-disable no-console */

"use strict";
// Constants and variables.
const axios = require("axios");
const config = require("./config.json");
const google = require("googleapis"); 

const getMilestones = async () => {
  try {
      let gitlabToken = process.env.GITLAB_RD_TOKEN;
      const getMilestonesURL = config.getMilestonesURL;
      const calendarURL = config.calendarURL;

      var options = {
        "headers" : {
          "PRIVATE-TOKEN" : gitlabToken
        }
      };
      var eventCal = google.getCalendarById(calendarURL);
      var cutoffDeleted = 0;
      var cutoffAdded = 0;
      var beginDeleted = 0;
      var beginAdded = 0;

      var dataMilestone = [];
      // Populate all milestones.
      UrlFetchApp.get
      for (var page = 1; page < 2; page++) {
        var response2 = UrlFetchApp.fetch(getMilestonesURL, options);
        // Parse the JSON reply
        var json = response2.getContentText();
        dataMilestone = dataMilestone.concat(JSON.parse(json));
        if (json.length < 100 ) {
          break;
        }
      }

      var bFound = false;
      var bExpired;

      dataMilestone.forEach(function(elem,i) {
        // (Re)set variables
        bExpired = elem["expired"];
        bFound = false;
        bStartDtFound = false; 

        // If expired = false, then delete/insert event otherwise, skip it as it's no longer an active milestone to update.
        if (bExpired == false) {

          /* Don't create a calendar event, unless we have a due date set for current milestone */
          var dueDate = new Date(elem["due_date"]);
          // Set the UTC timestamp regardless of where this is executed from.
          var utcDueDate = new Date(dueDate.getTime() + dueDate.getTimezoneOffset() * 60000);

          if (dueDate === "" || dueDate === null) { 
            bFound = false; 
          }
          else {
            bFound = true;
          }

          if (bFound = true) {
            var existingEvents = eventCal.getEventsForDay(utcDueDate, {search: 'This is the cutoff date for ' + elem["title"] + ' milestone.'});
            if (existingEvents.length > 0) {
              for (y=0; y<existingEvents.length; y++) {
                existingEvents[y].deleteEvent();
                cutoffDeleted++;
              }
            };

            if (dueDate) { 
              var event = eventCal.createAllDayEvent(elem["title"] + " cutoff", utcDueDate, {description: "This is the cutoff date for " + elem["title"] + " milestone."});
              cutoffAdded++;
            };
          }   

          /// LOGIC for setting begin date for milestone
          /* Don't create a calendar event, unless we have a due date set for current milestone */
          var startDate = new Date(elem["start_date"]);
          // Set the UTC timestamp regardless of where this is executed from.
          var utcStartDate = new Date(startDate.getTime() + startDate.getTimezoneOffset() * 60000);

          if (startDate === "" || startDate === null) { 
            bStartDtFound = false; 
          }
          else {
            bStartDtFound = true;
          }

          if (bStartDtFound = true) {
            var existingStartDtEvents = eventCal.getEventsForDay(utcStartDate, {search: 'This is the start date for ' + elem["title"] + ' milestone.'});
            if (existingStartDtEvents.length > 0) {
              for (y=0; y<existingStartDtEvents.length; y++) {
                existingStartDtEvents[y].deleteEvent();
                beginDeleted++;
              }
            };

            if (startDate) { 
              var eventStartDt = eventCal.createAllDayEvent(elem["title"] + " start", utcStartDate, {description: "This is the start date for " + elem["title"] + " milestone."});
              beginAdded++;
            };
          }
        }
      })

      console.log("Calendar successfully synced. " + beginDeleted.toString() + " milestone starts deleted. " + beginAdded.toString() +" milestone starts added.");
      console.log("Calendar successfully synced. " + cutoffDeleted.toString() + " milestone cut-offs deleted. " + cutoffAdded.toString() +" milestone cut-offs added.");
  
    }
  
  catch (err) {
    console.log(err);
  } 
};

getMilestones();
